# Style Transfer using a conditional Convolutional Variational Autoencoder

> This project is part of the Deep Learning Course 2021.

## Project Setup
This project is based on *Python* and *Tensorflow*. Please make sure you have `Python=3.8.5` with the following dependencies:
```
numpy=1.19.2
sklearn=0.23.2
tensorflow=2.4.0
tensorboard=2.4.0
tensorflow_probability=0.12.0
```

**Note: All scripts are intended to run on the ETH Euler cluster where they can easily be setup with the following command:**

```
$ env2lmod && module load gcc/6.3.0 python_gpu/3.8.5 hdf5/1.10.1
```

The *WikiArt* dataset is used which can be downloaded [here](https://github.com/cs-chan/ArtGAN/tree/master/WikiArt%20Dataset). It is expected to be at the location `data` by default, but the path can easily be changed at the top of `main.py` (for instance on Euler you might want to change that to your scratch directory). The first time the script is run the images will be transformed and saved in *npz* format at the same location. Our smaller variation of the dataset is already contained in the `data` folder and will be loaded automatically.


## Usage

### Tuning

To run one tuning session on the main model and save all logs for Tensorboard inside the directory `logs` run
```
$ python3 main.py
```

The logs can be viewed locally by synchronizing the `logs` folder and then hosting it with Tensorboard. Each tuning session is assigned an experiment ID, which can be specified in `main.py` as well as a timestamp. Each run within the session is then enumerated by a run ID. The full log path is thus `logs/{experimentID}/{timestamp}/{runID}`. Additionally, inference results will be produced for the best model within a session and stored to `logs/{experimentID}/{timestamp}/images` and the logs in the experiment folder are compatible with the *HParams* plugin of Tensorboard. If the main script is run in the current state it will only run one model with the best hyperparameters found. 


## Model Architecture

The model at hand is a conditional convolutional variational autoencoder which models the conditional marginal likelihood $`p(\mathbf{x}|s)`$ of images $`\mathbf{x}`$ given style labels $`s`$.

The encoder uses convolutional feature extraction blocks on images and dense feature maps on styles, which are then concatenated, to model the conditional posterior $`q(\mathbf{z}|\mathbf{x},s)`$ of latent variables $`\mathbf{z}`$, which are assumed to be standard normally distributed $`\mathbf{z} \sim \mathcal{N}(0, \mathbf{1})`$.

The decoder models the conditional likelihood given the latent sample $`p(\mathbf{x}|\mathbf{z},s)`$. The likelihood is assumed to be independently Gaussian $`\mathcal{N}(\boldsymbol{\mu}, \sigma^2\mathbf{1})`$, where the standard deviation $`\sigma`$ is an additional learnable parameter [[σ-VAE](https://arxiv.org/abs/2006.13202)]. This allows for reconstruction with different style $`s'`$ by sampling from the marginal distribution $`\int q(\mathbf{z}|\mathbf{x}, s) p(\mathbf{x}|\mathbf{z}, s') \text{d}\mathbf{z}`$, as well as conditional generative modelling by using $`\int p(\mathbf{z})p(\mathbf{x}|\mathbf{z}, s') \text{d}\mathbf{z}`$ instead.

![](img/architecture.png)

