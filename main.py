import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import io
import sys
import random
import numpy as np
import tensorflow as tf
from datetime import datetime
from sklearn.model_selection import train_test_split
from tensorboard.plugins.hparams import api as hp

from src.preprocessing.loading import create_dataset, load_data
from src.preprocessing.transformations import scale_image
from src.preprocessing.augmentation import augment_flips_rotations
from src.metrics.visualizations import log_reconstruction, log_generation, log_style_transfer
from src.model.cvae import define_model
from src.tuner import Tuner


tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

SEED = 163
RANDOM = random.Random(SEED)
DATAPATH = 'data'
ORACLEPATH = os.path.join('models', 'painter_classifier_model_ResNet-50_dense_32.h5')

if __name__ == '__main__':
    ########################################################
    ### LOAD DATA
    ########################################################
    
    if not os.path.exists(DATAPATH) or not os.path.isdir(DATAPATH):
        print(f"*** Data not available at {DATAPATH}. Exiting...")
        sys.exit(1)
    
    if not os.path.exists(ORACLEPATH):
        print(f"*** Oracle not available at {ORACLEPATH}. Exiting...")
        sys.exit(1)

    oracle = tf.keras.models.load_model(ORACLEPATH)
    #oracle.summary()

    if not os.path.exists(os.path.join(DATAPATH, 'dataset.npz')):
        create_dataset(DATAPATH, size=112)
    data, labels, styles = load_data(DATAPATH)
    train, test, s_train, s_test = train_test_split(data, labels, test_size=0.2, stratify=labels, random_state=SEED)
    assert train.shape[0] == s_train.shape[0] and test.shape[0] == s_test.shape[0]

    print(f"*** Training dataset loaded with shape {train.shape}")
    print(f"*** Validation dataset loaded with shape {test.shape}")
    print(f"*** Styles: {', '.join(styles)}")


    ########################################################
    ### TRANSFORM AND AUGMENT DATA
    ########################################################

    train, test = scale_image(train), scale_image(test)
    #train, s_train = augment_flips_rotations(train, s_train)

    n_styles = np.max(s_train) + 1
    s_train, s_test = tf.one_hot(s_train, n_styles), tf.one_hot(s_test, n_styles)


    ########################################################
    ### BUILD AND TUNE MODEL
    ########################################################

    # [-4.6, 1.2, 12, 2, 1, 3, 5, 12, 12, 13, 3, 3, 3, 2, 2, 2]
    # [-15921, -10503, 14.167, 14.045]

    hparams = [
        hp.HParam('architecture', hp.Discrete(['σ-VAE'])),
        hp.HParam('likelihood', hp.Discrete(['sigma'])),
        hp.HParam('output_activation', hp.Discrete(['linear'])),
        hp.HParam('log_lr', hp.RealInterval(-4., -4.)),
        hp.HParam('log_beta', hp.RealInterval(1., 1.)),
        hp.HParam('log_cce', hp.RealInterval(3.5, 3.5)),
        hp.HParam('latent_dim', hp.IntInterval(32, 32)),
        hp.HParam('feature_layers', hp.IntInterval(2, 2)),
        hp.HParam('style_layers', hp.IntInterval(1, 1)),
        hp.HParam('encoder_layers', hp.IntInterval(2, 2)),
        hp.HParam('decoder_layers', hp.IntInterval(4, 4)),
        hp.HParam('feature_filters', hp.IntInterval(16, 16)),
        hp.HParam('encoder_filters', hp.IntInterval(32, 32)),
        hp.HParam('decoder_filters', hp.IntInterval(32, 32)),
        hp.HParam('feature_kernel_size', hp.IntInterval(3, 3)),
        hp.HParam('encoder_kernel_size', hp.IntInterval(3, 3)),
        hp.HParam('decoder_kernel_size', hp.IntInterval(3, 3)),
        hp.HParam('feature_multiple', hp.IntInterval(2, 2)),
        hp.HParam('encoder_multiple', hp.IntInterval(2, 2)),
        hp.HParam('decoder_multiple', hp.IntInterval(2, 2))
    ]
    metrics = [
        hp.Metric('epoch_loss', group='train', display_name='Train ELBO'),
        hp.Metric('epoch_loss', group='validation', display_name='ELBO'),
        hp.Metric('epoch_kld', group='train', display_name='Train KLD'),
        hp.Metric('epoch_kld', group='validation', display_name='KLD')
    ]

    log_dir = 'logs'
    ex_name = os.path.join('ex01', datetime.now().strftime("%y%m%d-%H%M%S"))

    tuner = Tuner((train, s_train), (test, s_test), define_model, hparams, metrics, log_dir, oracle, seed=SEED)
    tuner.tune(ex_name, runs=1, epochs=1000)

    if tuner.best_model is None:
        print('\n*** No model gave finite results. Report omitted.')
        sys.exit(0)


    ########################################################
    ### INFERENCE WITH BEST MODEL
    ########################################################

    def log_inference(data, s_data, model, dirname):
        idx = np.argmax(s_data, axis=0)
        idx2 = np.argmax(s_data[np.max(idx)+1:], axis=0)
        example_batch = np.concatenate([data[idx], data[np.max(idx)+1:][idx2]])
        style_batch = tf.one_hot(list(range(n_styles))*2, n_styles)

        # Visualize reconstructions from latent means
        log_reconstruction(model, example_batch, style_batch, os.path.join(log_dir, ex_name, dirname))

        # Visualize style transfer
        log_style_transfer(model, example_batch, tf.one_hot(range(n_styles), n_styles), os.path.join(log_dir, ex_name, dirname))

        # Visualize generative sampling
        log_generation(model, tf.one_hot(range(n_styles), n_styles), os.path.join(log_dir, ex_name, dirname))

    # Log inference on train and test set
    log_inference(train, s_train, tuner.best_model, 'train')
    log_inference(test, s_test, tuner.best_model, 'test')


    ########################################################
    ### PRINT BEST RESULTS
    ########################################################

    print('\n*** Best hyperparameters found:')
    for param, value in tuner.best_params.items():
        print(f"{param}: {value}")

    print(f"\n*** Best ELBO: {tuner.min_loss}")
    print(f"*** Best BPD: {tuner.min_loss/train.shape[1]/train.shape[2]/np.log(2)}")
