import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.keras import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import UpSampling2D
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow_probability.python.layers import KLDivergenceRegularizer, IndependentNormal, DistributionLambda
from tensorflow_probability.python.distributions import Independent, Normal, Kumaraswamy, kl_divergence

from src.model.feature_extraction import FeatureExtractionBlock, StyleExtractionBlock
from src.model.cae import Encoder, Decoder
from src.metrics.losses import neg_log_likelihood


def make_normal_distr(log_scale, reinterpreted_batch_ndims=1):
    return lambda loc: Independent(
            Normal(tf.math.sigmoid(loc), tf.math.exp(log_scale)),
            reinterpreted_batch_ndims=reinterpreted_batch_ndims)


def make_kumaraswamy_distr(cdim, reinterpreted_batch_ndims=1):
    return lambda loc: Independent(
            Kumaraswamy(tf.math.softplus(loc[..., :cdim]), tf.math.softplus(loc[..., cdim:])),
            reinterpreted_batch_ndims=reinterpreted_batch_ndims)


class VAE(Model):
    def __init__(self, shape, n_styles, oracle, latent_dim=32, activation='swish', output_activation='linear', likelihood='sigma', log_beta=0., log_cce = 0.,
                 encoder_layers=1, encoder_filters=4, encoder_kernel_size=3, encoder_multiple=1,
                 decoder_layers=2, decoder_filters=4, decoder_kernel_size=3, decoder_multiple=1,
                 feature_layers=3, feature_filters=4, feature_kernel_size=3, feature_multiple=1,
                 style_layers=1, **kwargs):
        super(VAE, self).__init__()

        # Define feature maps
        self._image_features = FeatureExtractionBlock(
                layers=feature_layers, filters=feature_filters, kernel_size=feature_kernel_size, multiple=feature_multiple)
        feature_shape = self._image_features.compute_output_shape((None, *shape))
        self._style_features = StyleExtractionBlock((feature_shape[1], feature_shape[2], feature_shape[3] // 2), activation=activation, layers=style_layers)
 
        # Define encoder and posterior
        self._prior = Independent(Normal(tf.zeros(latent_dim), scale=1), reinterpreted_batch_ndims=1)
        self._sampler = IndependentNormal(latent_dim, activity_regularizer=KLDivergenceRegularizer(self._prior, weight=latent_dim*10**log_beta))
        self._encoder = Encoder(latent_dim=self._sampler.params_size(latent_dim), activation=activation,
                               layers=encoder_layers, filters=encoder_filters, kernel_size=encoder_kernel_size, multiple=encoder_multiple)

        # Define decoder and likelihood
        self.__log_scale = None
        if likelihood == 'standard':
            param_shape = shape
            self.__log_scale = tf.Variable(0., trainable=False)
            self._connect = DistributionLambda(make_normal_distr(self.__log_scale, reinterpreted_batch_ndims=len(shape)))
        elif likelihood == 'sigma':
            param_shape = shape
            self.__log_scale = tf.Variable(-1.8, trainable=True)
            self._connect = DistributionLambda(make_normal_distr(self.__log_scale, reinterpreted_batch_ndims=len(shape)))
        elif likelihood == 'kumaraswamy':
            cdim = shape[-1]
            param_shape = (*shape[:-1], 2*cdim)
            self._connect = DistributionLambda(make_kumaraswamy_distr(cdim, reinterpreted_batch_ndims=len(shape)))
        self._decoder = Decoder(param_shape, latent_dim=latent_dim+n_styles, activation=activation, output_activation=output_activation,
                               layers=decoder_layers, filters=decoder_filters, kernel_size=decoder_kernel_size, multiple=decoder_multiple)
       
        # Define oracle
        self._up = UpSampling2D(size=(2, 2))
        self._oracle = oracle
        self._oracle.trainable = False
        self._cce = CategoricalCrossentropy()
        self._log_cce = 10 ** log_cce

    def call(self, inputs, training=False):
        # Map features
        xi, s = inputs
        xi, xs = self._image_features(xi, training=training), self._style_features(s, training=training)
        x = tf.concat([xi, xs], axis=-1)

        # Encode
        x = self._encoder(x, training=training)

        # Sample posterior
        x = self._sampler(x)
        self.add_metric(kl_divergence(x, self._prior), 'kld')
        self.add_metric(tf.math.reduce_mean(x.variance()), 'disp')

        # Decode
        x = tf.concat([x, s], axis=-1)
        x = self._decoder(x, training=training)

        # Sample likelihood
        result = self._connect(x)
        if self.__log_scale is not None:
            self.add_metric(tf.math.exp(self.__log_scale), 'sigma')
        
        # call oracle
        x = self._up(result)
        x = self._oracle(x)
        cce = self._log_cce * self._cce(s, x)
        self.add_loss(cce)
        self.add_metric(cce, name='cce')

        return result

    def reconstruct(self, x, s, sample_posterior=True):
        xi, xs = self._image_features(x), self._style_features(s)
        x = tf.concat([xi, xs], axis=-1)
        x = self._encoder(x)
        x = self._sampler(x)
        if not sample_posterior:
            x = x.mean()
        x = tf.concat([x, s], axis=-1)
        x = self._decoder(x)
        x = self._connect(x)
        return x

    def generate(self, s):
        x = self._prior.sample()
        x = tf.concat([x, s], axis=-1)
        x = tf.expand_dims(x, axis=0)
        x = self._decoder(x)
        x = self._connect(x)
        return x


def define_model(shape, n_styles, oracle, hparams):
    model = VAE(shape, n_styles, oracle, **hparams)
    lr = 10**hparams['log_lr'] if 'log_lr' in hparams else 0.0001
    model.compile(optimizer=Adam(lr=lr), loss=neg_log_likelihood)
    return model

