import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Layer, InputLayer, Flatten, Reshape, Dense, Conv2D, Conv2DTranspose

from src.model.feature_extraction import MultiConv, MultiConvTranspose, FeatureExtractionBlock, StyleExtractionBlock
from src.metrics.losses import maximum_distance


class Encoder(Layer):
    def __init__(self, latent_dim=32, activation='swish', layers=1, filters=4, kernel_size=3, multiple=1):
        super(Encoder, self).__init__(name='Encoder')
        self._convs = [MultiConv(filters=2**i*filters, kernel_size=kernel_size, multiple=multiple, activation=activation) 
                      for i in range(layers)]
        self._downs = [Conv2D(filters=2**i*filters, kernel_size=kernel_size+2, activation=activation, strides=2, padding='same')
                      for i in range(layers)]
        self._flatten = Flatten()
        self._out_layer = Dense(latent_dim, activation='linear')

    def call(self, x, training=False):
        for conv, down in zip(self._convs, self._downs):
            x = conv(x, training=training)
            x = down(x)
        x = self._flatten(x)
        x = self._out_layer(x)
        return x


class Decoder(Layer):
    def __init__(self, output_shape, latent_dim=32, activation='swish', output_activation='linear', layers=1, filters=4, kernel_size=3, multiple=1):
        super(Decoder, self).__init__(name='Decoder')
        input_size = output_shape[0] // 2**layers
        self._input = Dense(input_size*input_size)
        self._reshape_in = Reshape([input_size, input_size, 1])
        self._ups = [Conv2DTranspose(filters=2**i*filters, kernel_size=kernel_size+2, activation=activation, strides=2, padding='same')
                    for i in range(layers)[::-1]]
        self._convs = [MultiConvTranspose(filters=2**i*filters, kernel_size=kernel_size, activation=activation) 
                      for i in range(layers)[::-1]]
        self._out_layer = Conv2D(filters=output_shape[2], kernel_size=1, activation=output_activation)

    def call(self, x, training=False):
        x = self._input(x)
        x = self._reshape_in(x)
        for conv, up in zip(self._convs, self._ups):
            x = up(x)
            x = conv(x, training=training)
        x = self._out_layer(x)
        return x


class Autoencoder(Model):
    def __init__(self, shape, n_styles, latent_dim=32, activation='swish',
                 encoder_layers=2, encoder_filters=4, encoder_kernel_size=3, encoder_multiple=1,
                 decoder_layers=2, decoder_filters=4, decoder_kernel_size=3, decoder_multiple=1,
                 feature_layers=3, feature_filters=4, feature_kernel_size=3, feature_multiple=1, 
                 style_layers=1, **kwargs):
        super(Autoencoder, self).__init__()
        self._image_features = FeatureExtractionBlock(
                layers=feature_layers, filters=feature_filters, kernel_size=feature_kernel_size, multiple=feature_multiple)
        feature_shape = self._image_features.compute_output_shape((None, *shape))
        self._style_features = StyleExtractionBlock((feature_shape[1], feature_shape[2], 1), activation=activation, layers=style_layers)
        self._encoder = Encoder(latent_dim=latent_dim, activation=activation,
                               layers=encoder_layers, filters=encoder_filters, kernel_size=encoder_kernel_size, multiple=encoder_multiple)
        self._decoder = Decoder(output_shape=shape, latent_dim=latent_dim+n_styles, activation=activation,
                               layers=decoder_layers, filters=decoder_filters, kernel_size=decoder_kernel_size, multiple=decoder_multiple)

    def call(self, inputs, training=False):
        xi, s = inputs
        x, xs = self._image_features(xi, training=training), self._style_features(s, training=training)
        x = tf.concat([x, xs], axis=-1)
        x = self._encoder(x, training=training)
        x = tf.concat([x, s], axis=-1)
        x = self._decoder(x, training=training)
        x = tf.math.sigmoid(x)
        self.add_metric(maximum_distance(xi, x), name='max')
        return x


def define_model(shape, n_styles, hparams):
    model = Autoencoder(shape, n_styles, **hparams)
    model.compile(optimizer='rmsprop', loss='mse')
    return model

