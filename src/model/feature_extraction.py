from tensorflow.keras import Model
from tensorflow.keras.layers import Layer, Dense, Reshape, Conv2D, Conv2DTranspose, MaxPool2D, BatchNormalization


class MultiConv(Model):
    def __init__(self, filters=4, kernel_size=3, activation='swish', multiple=1):
        super(MultiConv, self).__init__(name='MultiConv')
        self._convs = [Conv2D(filters=filters, kernel_size=kernel_size, activation=activation, padding='same') for _ in range(multiple)]
        self._batchnorm = BatchNormalization()

    def call(self, x, training=False):
        for conv in self._convs:
            x = conv(x)
        x = self._batchnorm(x, training=training)
        return x


class MultiConvTranspose(Layer):
    def __init__(self, filters=4, kernel_size=3, activation='swish', multiple=1):
        super(MultiConvTranspose, self).__init__(name='MultiConvTranspose')
        self._convs = [Conv2DTranspose(filters=filters, kernel_size=kernel_size, activation=activation, padding='same')
                      for _ in range(multiple)]
        self._batchnorm = BatchNormalization()

    def call(self, x, training=False):
        for conv in self._convs:
            x = conv(x)
        x = self._batchnorm(x, training=training)
        return x


class FeatureExtractionBlock(Layer):
    def __init__(self, layers=3, filters=4, kernel_size=3, activation='swish', multiple=1):
        super(FeatureExtractionBlock, self).__init__(name='FeatureExtractionBlock')
        self._convs = [MultiConv(filters=2**i*filters, kernel_size=kernel_size, activation=activation, multiple=multiple)
                      for i in range(layers)]
        self._pools = [MaxPool2D(pool_size=2)] * layers

    def call(self, x, training=False):
        for conv, pool in zip(self._convs, self._pools):
            x = conv(x, training=training)
            x = pool(x)
        return x


class StyleExtractionBlock(Layer):
    def __init__(self, output_shape, activation='swish', layers=1):
        super(StyleExtractionBlock, self).__init__(name='StyleExtractionBlock')
        dims = output_shape[0]*output_shape[1]*output_shape[2]
        self._denses = [Dense(dims//2**i, activation=activation) for i in range(1, layers)[::-1]]
        self._output_layer = Dense(dims, activation='linear')
        self._reshape = Reshape(output_shape)

    def call(self, x, training=False):
        for dense in self._denses:
            x = dense(x)
        x = self._output_layer(x)
        x = self._reshape(x)
        return x

