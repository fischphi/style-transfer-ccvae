import numpy as np


def augment_flips_rotations(data, s_data):
    flipped = np.concatenate([np.flip(data, axis=1), np.flip(data, axis=2)])
    rotated = np.concatenate([np.rot90(data, 1, axes=(1,2)),
                            np.rot90(data, 2, axes=(1,2)),
                            np.rot90(data, 3, axes=(1,2))])
    data = np.concatenate([data, flipped, rotated])
    s_data = np.repeat(s_data, 6)
    return data, s_data

