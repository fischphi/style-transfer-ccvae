import numpy as np


def scale_image(data, min_value=0., max_value=255.):
    return (data - min_value) / (max_value - min_value)


def clip_open(data, eps=1e-4):
    return np.clip(data, eps, 1-eps)

