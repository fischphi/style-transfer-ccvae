import os
import numpy as np
from functools import partial
from PIL import Image
from multiprocessing import Pool


def load_image(fname, size):
    image = np.array(Image.open(fname).resize((size, size)), dtype=np.float32)
    return image


def load_data(path):
    dataset = np.load(os.path.join(path, 'dataset.npz'))
    return dataset['data'], dataset['labels'], dataset['styles']


def create_dataset(path, size=64):
    styles = [s for s in os.listdir(path) if os.path.isdir(os.path.join(path, s))]
    image_sets = []
    label_sets = []
    for idx, d in enumerate([os.path.join(path, s) for s in styles]):
        files = [os.path.join(d, f) for f in os.listdir(d)]
        with Pool() as pool:
            images = pool.map(partial(load_image, size=size), files)
            image_sets.append(np.stack(images, axis=0))
            label_sets.append(np.repeat(idx, repeats=len(images)))

    np.savez(os.path.join(path, 'dataset.npz'), 
            data=np.concatenate(image_sets), 
            labels=np.concatenate(label_sets), 
            styles=np.array(styles)
    )

