import os
import random
import shutil
import numpy as np
import tensorflow as tf
from tensorflow.data import Dataset
from tensorflow.summary import create_file_writer
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping, TensorBoard
from tensorboard.plugins.hparams import api as hp


class Tuner():
    """A tuner object that will train models with randomly sampled parameters and log them to TensorBoard."""
    def __init__(self, train, test, model_fn, hparams, metrics, log_dir, oracle, seed=0):
        """Initialize tuner object.

        Parameters:
            train (tuple): Images and one-hot encoded styles for training
            test (tuple): Images and one-hot encoded styles for validation
            model_fn (func): Function used to define the model taking two arguments:
                input_shape (tuple): shape of the input tensor
                params (dict): dictionary of model hyperparameters
            hparams (class hparams.HParams): HParams object to sample parameters from
            metrics (class hparams.Metrics): Metrics object to log
            log_dir (str): log directory name for TensorBoard logs
            seed (int): seed for random number generator
        """

        self.shape = train[0].shape[1:]
        self.n_styles = int(tf.shape(train[1])[-1])
        self.train = Dataset.from_tensor_slices((train, train[0])).shuffle(1000).batch(32)
        self.test = Dataset.from_tensor_slices((test, test[0])).batch(32)
        self.model_fn = model_fn
        self.hparams = hparams
        self.metrics = metrics
        self.log_dir = log_dir
        self.oracle = oracle
        self.random = random.Random(seed)

        self.min_loss = float('inf')
        self.best_params = None
        self.best_model = None

    def _run(self, params, run_dir, epochs):
        """Sample one set of hyperparameters and train the model with them.

        Parameters:
            params (dict): dictionary of hyperparameters to give to model_fn
            run_dir (str): name of the directory to write TensorBoard logs
            epochs (int): number of epochs to train the model for

        Returns: None
        """

        weights_dir = os.path.join(run_dir, 'weights/')
        model = self.model_fn(self.shape, self.n_styles, self.oracle, params)

        checkpoint = ModelCheckpoint(weights_dir, save_best_only=True, save_weights_only=True)
        stopping = EarlyStopping(patience=200)
        tensorboard = TensorBoard(log_dir=run_dir)
        hpboard = hp.KerasCallback(run_dir, params)

        print(f"\n*** Starting run and logging to {run_dir}")
        model.fit(self.train, epochs=epochs, verbose=2, validation_data=self.test,
                 callbacks=[checkpoint, stopping, tensorboard, hpboard])

        print('Validation:')
        best_model = self.model_fn(self.shape, self.n_styles, self.oracle, params)
        best_model.load_weights(weights_dir)
        metrics = best_model.evaluate(self.test, verbose=2)
        return model, metrics[0]

    def tune(self, ex_name, runs=10, epochs=1, keep_weights=False):
        """Sample multiple sets of hyperparameters and train the model with each of them.

        Parameters:
            ex_name (str): experiment name used as subdirectory for logs
            runs (int): number of hyperparameter sets to be sampled and used for training
            epochs (int): number of epochs to train the model for
            keep_weights (bool): if True all weights of every trained model are saved

        Returns: None
        """

        model_dir = os.path.join(self.log_dir, 'model')
        ex_dir = os.path.join(self.log_dir, ex_name)
        with create_file_writer(ex_dir).as_default():
            hp.hparams_config(hparams=self.hparams, metrics=self.metrics)

        for i in range(runs):
            run_dir = os.path.join(ex_dir, f"{i:04d}")
            params = {h.name: h.domain.sample_uniform(self.random) for h in self.hparams}
            model, loss = self._run(params, run_dir, epochs)

            if loss < self.min_loss:
                self.min_loss = loss
                self.best_params = params
                self.best_model = model

            if not keep_weights:
                shutil.rmtree(os.path.join(run_dir, 'weights'))

