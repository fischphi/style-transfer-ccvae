import os
import numpy as np
import tensorflow as tf
from tensorflow.summary import create_file_writer


def log_reconstruction(model, example_batch, style_batch, log_dir):
    examples = np.concatenate(list(example_batch), axis=-2)
    pmean_batch = model.reconstruct(example_batch, style_batch, sample_posterior=False).mean()
    pmeans = np.concatenate(list(pmean_batch), axis=-2)
    mean_batches = [model.reconstruct(example_batch, style_batch, sample_posterior=True).mean() for _ in range(3)]
    mean_batch = np.concatenate(mean_batches, axis=-3)
    means = np.concatenate(list(mean_batch), axis=-2)
    sample_batches = [model.reconstruct(example_batch, style_batch, sample_posterior=True).sample() for _ in range(3)]
    sample_batch = np.concatenate(sample_batches, axis=-3)
    samples = np.concatenate(list(sample_batch), axis=-2)
    image = np.concatenate((examples, pmeans, means, samples), axis=-3)
    with create_file_writer(os.path.join(log_dir, 'images')).as_default():
        tf.summary.image('Reconstruction (original, mean, samples)', image[np.newaxis, ...], step=0)


def log_style_transfer(model, example_batch, styles, log_dir):
    examples = np.concatenate(list(example_batch), axis=-2)
    images = []
    batch_size = tf.shape(example_batch)[:1]
    for style in tf.unstack(styles):
        style_batch = tf.transpose(tf.repeat(tf.expand_dims(style, axis=-1), batch_size, axis=1))
        transferred_batch = model.reconstruct(example_batch, style_batch, sample_posterior=True).mean()
        images.append(np.concatenate(transferred_batch, axis=-2))
    image = np.concatenate((examples, *images), axis=-3)
    with create_file_writer(os.path.join(log_dir, 'images')).as_default():
        tf.summary.image('Style Transfer (original, styles)', image[np.newaxis, ...], step=0)


def log_generation(model, style_batch, log_dir):
    images = []
    for style in tf.unstack(style_batch):
        generated_batch = [model.generate(style).mean() for _ in range(6)]
        images.append(np.concatenate(generated_batch, axis=-2))
    image = np.concatenate(images, axis=-3)
    with create_file_writer(os.path.join(log_dir, 'images')).as_default():
        tf.summary.image('Generated Samples', image, step=0)

