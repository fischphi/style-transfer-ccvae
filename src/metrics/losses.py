import tensorflow as tf


def maximum_distance(x_true, x_pred):
    return tf.math.reduce_max(tf.math.abs(x_true - x_pred))


def neg_log_likelihood(x_true, rx_pred):
    return -rx_pred.log_prob(x_true)

